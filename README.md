Friendly Captcha Bundle
=============
The `FriendlyCaptchaBundle` provides the a `friendly-captcha` form field for Symfony  


### What now?

Documentation! The documentation for this bundle is available in the `docs`
directory of the bundle:

* Read the [FriendlyCaptchaBundle documentation](https://git.personalwerk.net/pwrk/jobboard/friendly-captcha-bundle/-/blob/master/docs/installation.md)
    * [Installation](https://git.personalwerk.net/pwrk/jobboard/friendly-captcha-bundle/-/blob/master/docs/installation.md)
    * [Configure](https://git.personalwerk.net/pwrk/jobboard/friendly-captcha-bundle/-/blob/master/docs/configure.md)
    * [Usage](https://git.personalwerk.net/pwrk/jobboard/friendly-captcha-bundle/-/blob/master/docs/usage.md)

This bundle's job is to integrate a friendly captcha form field. If you want you can also use the captcha service without the form field.
You can learn a lot more about how this library works by reading that library's
documentation.

## Maintainers

This library is maintained by the following people:
- @tschmidt


## Credits

Thanks to mum to make this happen :) 