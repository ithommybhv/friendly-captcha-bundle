Configure the FriendlyCaptchaBundle
=============================

So currently this bundle isn't public. So we don't have a recipe in the Symfony Recipes Repository.

To configure this bundle you need to create your own configuration file inside the config/packages directory:

Create a new file inside (YourAwesomeProject/config/packages) and call it friendly_captcha.yaml.
Inside the configuration you have to set the `apikey` and the `sitekey`. If you forget to enable friendlycaptcha, currently it will not do a request. So your form field will fail.

```yaml
friendly_captcha:
  isEnabled: true
  apikey: 'A1SXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'
  sitekey: 'FCXXXXXXXXXXX'
```



