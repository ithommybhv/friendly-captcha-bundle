Installing the FriendlyCaptchaBundle
=============================

Install the FriendlyCaptchaBundle is easy. Currently, the app isn't available at packagist or similar.
So you need to clone it wherever you want. Just make sure you define the right repository url in your composer.json file.

An example shows a part from our composer.json. We're using the bundle inside the lib directory. So our  composer.json looks like:

```json

"repositories": [
    {
      "type": "path",
      "url": "lib/FriendlyCaptchaBundle"
    }
  ]

```

You see our FriendlyCaptchaBundle is located inside the lib directory.
If you want to use the same directory feel free to clone it now inside your own lib directory.

Inside your lib directory run:
```
git clone git@git.personalwerk.net:pwrk/jobboard/friendly-captcha-bundle.git FriendlyCaptchaBundle
```

After cloning the repository you need to run:

```
composer require pwrk/friendly-captcha-bundle:@dev
```

Now symfony also should activate the bundle inside your bundles.yaml.

