Usage the FriendlyCaptchaBundle
=============================

When you create your own form class you just need creat a new field with our form type:

```php
use Pwrk\Bundle\FriendlyCaptchaBundle\Form\Type\FriendlyCaptchaType;
use Pwrk\Bundle\FriendlyCaptchaBundle\Validator\CaptchaIsTrue;

public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('friendly_captcha', FriendlyCaptchaType::class, [
                'constraints' => [
                    new CaptchaIsTrue(),
                ],
            ]);
    }
```

