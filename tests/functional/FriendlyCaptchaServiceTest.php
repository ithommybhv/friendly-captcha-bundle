<?php


namespace Pwrk\Bundle\FriendlyCaptchaBundle\Tests\Functional;


use http\Client\Request;
use PHPUnit\Framework\TestCase;
use Pwrk\Bundle\FriendlyCaptchaBundle\Service\FriendlyCaptchaService;
use Pwrk\Bundle\FriendlyCaptchaBundle\Tests\FriendlyCaptchaTestingKernel;


class FriendlyCaptchaServiceTest extends TestCase
{
    public function testWiredFriendlyCaptchaValidateIsFalse()
    {
        $kernel = new FriendlyCaptchaTestingKernel(['apikey' => '123456', 'sitekey' => '654321']);
        $kernel->boot();
        $container = $kernel->getContainer();

        /** @var FriendlyCaptchaService $friendlyCaptchaService */
        $friendlyCaptchaService = $container->get('pwrk_friendlycaptcha.fc.service');
        $this->assertInstanceOf(FriendlyCaptchaService::class, $friendlyCaptchaService);
        $this->assertFalse($friendlyCaptchaService->validate());
    }
}