<?php

namespace Pwrk\Bundle\FriendlyCaptchaBundle\Tests\Functional;

use PHPUnit\Framework\TestCase;
use Pwrk\Bundle\FriendlyCaptchaBundle\Service\FriendlyCaptchaService;
use Pwrk\Bundle\FriendlyCaptchaBundle\Tests\FriendlyCaptchaTestingKernel;

class ServiceTest extends TestCase
{
    public function testServiceWiring()
    {
        $kernel = new FriendlyCaptchaTestingKernel(
            ['apikey' => '123456', 'sitekey' => '654321']
        );
        $kernel->boot();
        $container = $kernel->getContainer();

        // Watch out it can be a different url!
        $this->assertSame('https://friendlycaptcha.com/api/v1/siteverify', $container->getParameter('friendly_captcha.endpointurl'));

        $this->assertFalse($container->getParameter('friendly_captcha.isEnabled'));
        $this->assertSame('123456', $container->getParameter('friendly_captcha.apikey'));
        $this->assertSame('654321', $container->getParameter('friendly_captcha.sitekey'));
    }
}