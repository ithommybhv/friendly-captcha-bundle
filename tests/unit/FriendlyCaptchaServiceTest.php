<?php


use PHPUnit\Framework\TestCase;
use Pwrk\Bundle\FriendlyCaptchaBundle\Service\FriendlyCaptchaService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class FriendlyCaptchaServiceTest extends TestCase
{

    public function testFriendlyCaptchaValidateIsTrue()
    {
        $responseMock = $this->createMock(ResponseInterface::class);
        $responseMock->method('getStatusCode')->willReturn(200);
        $responseMock->method('toArray')->willReturn(['success' => true]);

        $requestMock = $this->createMock(Request::class);
        $requestMock->method('get')->willReturn('x3');

        $httpClientMock = $this->createMock(HttpClientInterface::class);
        $httpClientMock->method('request')->willReturn($responseMock);

        $requestStackMock = $this->createMock(RequestStack::class);
        $requestStackMock->method('getMasterRequest')->willReturn($requestMock);

        $friendlyCaptcha = new FriendlyCaptchaService($httpClientMock, $requestStackMock, true, 'key123', 'site123', '');

        $this->assertTrue($friendlyCaptcha->validate());
    }

}