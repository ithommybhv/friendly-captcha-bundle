<?php

namespace Pwrk\Bundle\FriendlyCaptchaBundle\Tests;

use Pwrk\Bundle\FriendlyCaptchaBundle\FriendlyCaptchaBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Kernel;

class FriendlyCaptchaTestingKernel extends Kernel
{

    private $bundleConfig;

    public function __construct(array $bundleConfig)
    {
        parent::__construct('test', true);
        $this->bundleConfig = $bundleConfig;
    }

    public function registerBundles()
    {
        return [new FriendlyCaptchaBundle()];
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load(function (ContainerBuilder $containerBuilder) {
            $containerBuilder->register('http_client', CurlHttpClient::class);
            $containerBuilder->register('request_stack', RequestStack::class);
            $containerBuilder->loadFromExtension('friendly_captcha', $this->bundleConfig);
        });
    }

    public function getCacheDir()
    {
        return __DIR__ . '/cache/' . spl_object_hash($this);
    }
}