<?php

namespace Pwrk\Bundle\FriendlyCaptchaBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;

class FriendlyCaptchaType extends AbstractType
{
    /**
     * @var String
     */
    protected $siteKey;

    /**
     * @var String
     */
    private $apiKey;

    /**
     * @var bool
     */
    protected $enabled;


    /**
     * Construct.
     *
     * @param String $siteKey FriendlyCaptcha site key
     * @param Boolean $enabled FriendlyCaptcha status
     */
    public function __construct($siteKey, $apiKey, $enabled)
    {
        $this->siteKey = $siteKey;
        $this->apiKey = $apiKey;
        $this->enabled = $enabled;
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars = array_replace(
            $view->vars,
            [
                'pwrk_fcaptcha_sitekey' => $this->siteKey,
                'pwrk_fcaptcha_apikey' => $this->apiKey,
                'pwrk_fcaptcha_enabled' => $this->enabled,
            ]
        );
    }


    public function getSiteKey(): string
    {
        return $this->siteKey;
    }

    public function getApiKey(): string
    {
        return $this->apiKey;
    }
}