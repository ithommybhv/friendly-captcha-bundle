<?php


namespace Pwrk\Bundle\FriendlyCaptchaBundle\Service;


use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class FriendlyCaptchaService implements CaptchaInterface
{

    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    /**
     * @var bool
     */
    private $isEnabled;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $siteKey;

    /**
     * @var string
     */
    private $endpointUrl;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(
        HttpClientInterface $httpClient,
        RequestStack $requestStack,
        bool $isEnabled,
        string $apiKey,
        string $siteKey,
        string $endpointUrl
    )
    {
        $this->httpClient = $httpClient;
        $this->requestStack = $requestStack;
        $this->isEnabled = $isEnabled;
        $this->apiKey = $apiKey;
        $this->siteKey = $siteKey;
        $this->endpointUrl = $endpointUrl;
    }

    public function validate(): bool
    {

        if ($this->isEnabled) {
            $content = $this->requestEndpoint();

            if ($content['success']) {
                return true;
            }
        }

        return false;
    }

    protected function requestEndpoint(): array
    {
        $response = $this->httpClient
            ->request('POST', $this->endpointUrl, $this->buildOptions());

        //Todo Sollte bei mehr Zeit ausgebaut werden, Fehler > 200 etc.
        if (200 !== $response->getStatusCode()) {
            return ['success' => false];
        }

        return $response->toArray();
    }

    /**
     * Build request options
     *
     * @return array[]
     */
    protected function buildOptions(): array
    {
        $body = [
            'secret' => $this->apiKey,
            'sitekey' => $this->siteKey,
            'solution' => $this->getCaptchaSolution()
        ];

        return ['body' => $body];
    }

    /**
     * Get the current captcha value
     *
     * @return string
     */
    protected function getCaptchaSolution()
    {
        $masterRequest = $this->requestStack->getMasterRequest();
        $response = $masterRequest->get("frc-captcha-solution");

        return isset($response) ? trim($response) : '';
    }
}