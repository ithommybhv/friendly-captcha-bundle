<?php


namespace Pwrk\Bundle\FriendlyCaptchaBundle\Service;


interface CaptchaInterface
{
    public function validate(): bool;
}