<?php

namespace Pwrk\Bundle\FriendlyCaptchaBundle\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class CaptchaIsTrue extends Constraint
{
    /*
     * Any public properties become valid options for the annotation.
     * Then, use these in your validator class.
     *
     * Todo: Translation?
     */
    public $message = 'Bitte captcha bestätigen';
}
