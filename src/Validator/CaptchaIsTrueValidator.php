<?php

namespace Pwrk\Bundle\FriendlyCaptchaBundle\Validator;

use Pwrk\Bundle\FriendlyCaptchaBundle\Service\FriendlyCaptchaService;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class CaptchaIsTrueValidator extends ConstraintValidator
{
    /**
     * @var FriendlyCaptchaService
     */
    private $friendlyCaptchaService;

    public function __construct(FriendlyCaptchaService $friendlyCaptchaService)
    {
        $this->friendlyCaptchaService = $friendlyCaptchaService;
    }

    public function validate($value, Constraint $constraint)
    {
        /* @var $constraint CaptchaIsTrue */

        if ($this->friendlyCaptchaService->validate()) {
            return;
        }

        // TODO: Translation wenn ben.
        $this->context->buildViolation($constraint->message)
            ->setParameter('Captcha bestätigen', '$value')
            ->addViolation();
    }
}
