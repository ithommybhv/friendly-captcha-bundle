<?php

namespace Pwrk\Bundle\FriendlyCaptchaBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class FriendlyCaptchaExtension extends Extension
{

    public function load(array $configs, ContainerBuilder $container)
    {
        
        $configuration = $this->getConfiguration($configs, $container);
        $config = $this->processConfiguration($configuration, $configs);

        $yamlFileLoader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $yamlFileLoader->load('services.yml');

        foreach ($config as $key => $value) {
            $container->setParameter('friendly_captcha.' . $key, $value);
        }

        $this->registerWidget($container);
    }

    protected function registerWidget(ContainerBuilder $containerBuilder)
    {
        $templatingEngines = $containerBuilder->hasParameter('templating.engines')
            ? $containerBuilder->getParameter('templating.engines')
            : ['twig'];

        if (in_array('php', $templatingEngines)) {
            $formResource = 'FriendlyCaptchaBundle:Form';

            $containerBuilder->setParameter('templating.helper.form.resources', array_merge(
                $containerBuilder->getParameter('templating.helper.form.resources'),
                [$formResource]
            ));
        }

        if (in_array('twig', $templatingEngines)) {
            $formResource = '@FriendlyCaptcha/Form/pwrk_fcaptcha_widget.html.twig';

            $containerBuilder->setParameter('twig.form.resources', array_merge(
                $this->getTwigFormResources($containerBuilder),
                [$formResource]
            ));
        }
    }

    private function getTwigFormResources(ContainerBuilder $container)
    {
        if (!$container->hasParameter('twig.form.resources')) {
            return [];
        }
        return $container->getParameter('twig.form.resources');
    }
}