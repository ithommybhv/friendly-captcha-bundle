<?php


namespace Pwrk\Bundle\FriendlyCaptchaBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('friendly_captcha');
        $rootNode = $this->getRootNode($treeBuilder);

        $rootNode
            ->children()
            ->booleanNode('isEnabled')->defaultFalse()->info('Activate or deactivate friendly_captcha')->end()
            ->scalarNode('apikey')->isRequired()->info('FriendlyCaptcha apikey is required')->end()
            ->scalarNode('sitekey')->isRequired()->info('FriendlyCaptcha sitekey is required')->end()
            ->scalarNode('endpointurl')->defaultValue('https://friendlycaptcha.com/api/v1/siteverify')->info('If the url swaps you can set the new one')->end()
            ->end();

        return $treeBuilder;
    }

    /**
     * @param TreeBuilder $treeBuilder
     * @param string $childName
     *
     * @return NodeDefinition|ArrayNodeDefinition The root node (as an ArrayNodeDefinition when the type is 'array')
     */
    private function getRootNode(TreeBuilder $treeBuilder, string $childName = 'friendly_captcha'): NodeDefinition
    {
        $rootNode = null;

        /** Adds BC for symfony/config 4.1 and older **/
        if (method_exists($treeBuilder, 'getRootNode')) {
            $rootNode = $treeBuilder->getRootNode();
        } else {
            $rootNode = $treeBuilder->root($childName);
        }

        return $rootNode;
    }
}